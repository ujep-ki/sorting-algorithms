package io.techniktom.sortingalgorithms;

public enum AlgorithType {
    BUBLE("Buble"), INSERRT("Insert"), QUICK("Quick"), COUNTING("Counting"), COUNTING_DYNAMIC("CountingDynamic");

    public final String AlgoritmName;

    AlgorithType(String algorithmName) {
        AlgoritmName = algorithmName;
    }
}
