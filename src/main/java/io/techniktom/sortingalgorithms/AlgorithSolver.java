package io.techniktom.sortingalgorithms;

import io.techniktom.sortingalgorithms.algorithms.*;

public class AlgorithSolver {
    private Integer[] array;

    public AlgorithSolver(Integer[] array, AlgorithType type) {
        switch (type) {
            case BUBLE:
                this.array = new Bubble(array).sortArray();
                break;

            case INSERRT:
                this.array = new Insert(array).sortArray();
                break;

            case COUNTING:
                this.array = new Counting(array).sortArray();
                break;

            case COUNTING_DYNAMIC:
                this.array = new CountingDynamic(array).sortArray();
                break;

            case QUICK:
                this.array = new Quick(array).sortArray();
                break;
        }
    }

    public Integer[] getArray() {
        return array;
    }
}
