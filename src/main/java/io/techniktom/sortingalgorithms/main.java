package io.techniktom.sortingalgorithms;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class main {

    public static void main(String[] args) throws IOException {
        File numbersFile = new File("generated_numbers.txt");

        if (!numbersFile.exists()) {
            System.out.println("File with numbers don't exists!");
            System.out.println("Run command to generate them!");

            return;
        }

        BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in));

        AlgorithType[] types = AlgorithType.values();

        System.out.println("Select sort:");


        for (int i = 0; i < types.length; i++) {
            System.out.println(String.format("%s) %s", i + 1, types[i].AlgoritmName));
        }

        System.out.print("> ");
        String typeNumber = consoleReader.readLine();

        int typeIndex;
        try {
            typeIndex = Integer.parseInt(typeNumber);
        } catch (Exception e) {
            System.out.println("You did not entered number!");

            return;
        }

        BufferedReader numberFileReader = new BufferedReader(new InputStreamReader(new FileInputStream(numbersFile), StandardCharsets.UTF_8));

        List<String> arrayOnNumbers = numberFileReader.lines().collect(Collectors.toList());
        AlgorithType type = types[typeIndex - 1];


        List<Integer> numbers = new ArrayList<>();

        arrayOnNumbers.forEach(stringNumber -> {
            try {
                numbers.add(Integer.parseInt(stringNumber));
            } catch (NumberFormatException ignored) {}
        });

        System.out.println("Start sorting array by '" + type.AlgoritmName + "' sorting algorithm");

        long time = System.currentTimeMillis();

        AlgorithSolver algorithSolver = new AlgorithSolver(numbers.toArray(new Integer[numbers.size()]), type);

        System.out.println("Sorting taken: " + (System.currentTimeMillis() - time) + "ms");

        Integer[] sortedArray = algorithSolver.getArray();

        File outputFile = new File(type.name().toLowerCase()+".txt");

        if(outputFile.exists()) {
            outputFile.delete();
        }

        PrintWriter dataWriter = new PrintWriter(new OutputStreamWriter(new FileOutputStream(outputFile), StandardCharsets.UTF_8));

        for (int i = 0, sortedArrayLength = sortedArray.length; i < sortedArrayLength; i++) {
            Integer integer = sortedArray[i];
            if (integer != null)
                dataWriter.println(integer.toString());
        }

        dataWriter.flush();
        dataWriter.close();

        System.out.println("Data are written to file '"+outputFile.getAbsolutePath()+"'");
    }
}
