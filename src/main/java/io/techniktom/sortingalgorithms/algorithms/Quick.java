package io.techniktom.sortingalgorithms.algorithms;

import io.techniktom.sortingalgorithms.SortingStatic;

public class Quick extends SortingStatic<Integer> {

    public Quick(Integer[] unsortedArray) {
        super(unsortedArray);
    }

    @Override
    public Integer[] sortArray() {
        sort(array, 0, array.length - 1);

        return array;
    }

    private int partition(Integer[] arr, int low, int high) {
        int pivot = arr[high];
        int i = (low - 1);
        for (int j = low; j < high; j++) {
            if (arr[j] < pivot) {
                i++;

                swap(array, i, j);
            }
        }

        swap(arr, i + 1, high);

        return i + 1;
    }

    private void sort(Integer[] arr, int low, int high) {
        if (low < high) {
            int pi = partition(arr, low, high);

            sort(arr, low, pi - 1);
            sort(arr, pi + 1, high);
        }
    }
}
