package io.techniktom.sortingalgorithms.algorithms;

import io.techniktom.sortingalgorithms.SortingStatic;

public class Insert extends SortingStatic<Integer> {

    public Insert(Integer[] unsortedArray) {
        super(unsortedArray);
    }

    @Override
    public Integer[] sortArray() {
        for (int i = 0; i < array.length - 2; i++) {
            int j = i + 1;
            tmp = array[j];

            while (j > 0 && compare(tmp, array[j - 1]) > 0) {
                array[j] = array[j - 1];
                j--;
            }

            array[j] = tmp;
        }

        return array;
    }
}
